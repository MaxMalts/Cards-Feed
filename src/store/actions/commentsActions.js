import * as actionTypes from '@store/actionTypes.js';
import getCurDate from '@assets/helpers/getCurDate.js';

export const actionLoadCardComments = (cardId, comments) => ({
   type: actionTypes.loadCardComments,
   payload: {
       cardId,
       comments
   }
});

export const actionAddComment = (cardId, author, text) => ({
    type: actionTypes.addComment,
    payload: {
        cardId,
        author,
        text,
        date: getCurDate()
    }
});

export const actionDeleteComment = (cardId, id) => ({
    type: actionTypes.deleteComment,
    payload: {
        cardId,
        id
    }
});