openapi: 3.0.3

info:
  title: Cards Feed
  description: This is a cards feed
  contact:
    email: maltsev.ms@phystech.edu
  version: 1.0.0-SNAPSHOT

tags:
  - name: card
    description: Cards
  - name: comment
    description: Comments

paths:

  /cards:

    get:
      tags:
        - card
      summary: getCards
      description: Returns cards
      operationId: getCards
      security:
        - apiKey: []
      parameters:
        - $ref: '#/components/parameters/ApplicationName'
        - name: cursor
          description: Id of previous card (not included in response)
          in: query
          required: false
          schema:
            $ref: '#/components/schemas/CardId'
        - $ref: '#/components/parameters/Limit'
      responses:
        200:
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GetCardsResponse'
        400:
          $ref: '#/components/responses/Error'
        401:
          $ref: '#/components/responses/Error'
        500:
          $ref: '#/components/responses/Error'

  /card/{cardId}:

    get:
      tags:
        - card
      summary: getCard
      description: Returns card data
      operationId: getCard
      security:
        - apiKey: []
      parameters:
        - $ref: '#/components/parameters/ApplicationName'
        - name: cardId
          in: path
          description: Card identifier
          required: true
          schema:
            $ref: '#/components/schemas/CardId'
      responses:
        200:
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GetCardResponse'
        400:
          $ref: '#/components/responses/Error'
        401:
          $ref: '#/components/responses/Error'
        500:
          $ref: '#/components/responses/Error'

  /card:

    post:
      tags:
        - card
      summary: addCard
      description: Create a new card
      operationId: addCard
      security:
        - apiKey: []
      parameters:
        - $ref: '#/components/parameters/ApplicationName'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AddCardRequest'
      responses:
        200:
          description: Ok
        400:
          $ref: '#/components/responses/Error'
        401:
          $ref: '#/components/responses/Error'
        500:
          $ref: '#/components/responses/Error'

  /card/{cardId}/like:

    post:
      tags:
        - card
      summary: likeCard
      description: Add like on a card
      operationId: likeCard
      security:
        - apiKey: []
      parameters:
        - $ref: '#/components/parameters/ApplicationName'
        - name: cardId
          in: path
          description: Card identifier
          required: true
          schema:
            $ref: '#/components/schemas/CardId'
      responses:
        200:
          description: Ok
        400:
          $ref: '#/components/responses/Error'
        401:
          $ref: '#/components/responses/Error'
        500:
          $ref: '#/components/responses/Error'

  /card/{cardId}/unlike:  # or maybe better to add boolean parameter to /like?

    post: # or maybe better delete?
      tags:
        - card
      summary: unlikeCard
      description: Remove like from a card
      operationId: unlikeCard
      security:
        - apiKey: []
      parameters:
        - $ref: '#/components/parameters/ApplicationName'
        - name: cardId
          in: path
          description: Card identifier
          required: true
          schema:
            $ref: '#/components/schemas/CardId'
      responses:
        200:
          description: Ok
        400:
          $ref: '#/components/responses/Error'
        401:
          $ref: '#/components/responses/Error'
        500:
          $ref: '#/components/responses/Error'

  /card/{cardId}/comments:

    get:
      tags:
        - comment
      summary: getComments
      description: Get comments on a card
      operationId: getComments
      security:
        - apiKey: []
      parameters:
        - $ref: '#/components/parameters/ApplicationName'
        - name: cardId
          in: path
          description: Card identifier
          required: true
          schema:
            $ref: '#/components/schemas/CardId'
        - name: cursor
          description: Id of previous comment (not included in response)
          in: query
          required: false
          schema:
            $ref: '#/components/schemas/CommentId'
        - $ref: '#/components/parameters/Limit'
      responses:
        200:
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GetCommentsResponse'
        400:
          $ref: '#/components/responses/Error'
        401:
          $ref: '#/components/responses/Error'
        500:
          $ref: '#/components/responses/Error'

  /card/{cardId}/comment:

    post:
      tags:
        - comment
      summary: addComment
      description: Create a new comment
      operationId: addComment
      security:
        - apiKey: []
      parameters:
        - $ref: '#/components/parameters/ApplicationName'
        - name: cardId
          in: path
          description: Card identifier
          required: true
          schema:
            $ref: '#/components/schemas/CardId'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AddCommentRequest'
      responses:
        200:
          description: Ok
        400:
          $ref: '#/components/responses/Error'
        401:
          $ref: '#/components/responses/Error'
        500:
          $ref: '#/components/responses/Error'

  /comment/{commentId}/like:

    post:
      tags:
        - comment
      summary: likeComment
      description: Add like on a comment
      operationId: likeComment
      security:
        - apiKey: []
      parameters:
        - $ref: '#/components/parameters/ApplicationName'
        - name: commentId
          in: path
          description: Comment identifier
          required: true
          schema:
            $ref: '#/components/schemas/CommentId'
      responses:
        200:
          description: Ok
        400:
          $ref: '#/components/responses/Error'
        401:
          $ref: '#/components/responses/Error'
        500:
          $ref: '#/components/responses/Error'

  /comment/{commentId}/unlike:  # or maybe better to add boolean parameter to /like?

    post:  # or maybe better delete?
      tags:
        - comment
      summary: unlikeComment
      description: Remove like from a comment
      operationId: unlikeComment
      security:
        - apiKey: []
      parameters:
        - $ref: '#/components/parameters/ApplicationName'
        - name: commentId
          in: path
          description: Comment identifier
          required: true
          schema:
            $ref: '#/components/schemas/CommentId'
      responses:
        200:
          description: Ok
        400:
          $ref: '#/components/responses/Error'
        401:
          $ref: '#/components/responses/Error'
        500:
          $ref: '#/components/responses/Error'

components:

  securitySchemes:

    apiKey:
      type: apiKey
      in: header
      name: X-API-KEY

  parameters:

    ApplicationName:
      description: Calling application name
      name: x-app-name
      in: header
      required: true
      schema:
        type: string
        minLength: 1
      example: cards-feed

    Limit:
      name: limit
      description: Maximum number of cards returned
      in: query
      required: true
      schema:
        type: integer
        minimum: 1

  responses:

    Error:
      description: Error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorResponse'

  schemas:

    GetCardsResponse:
      type: object
      required:
        - lastCardId
        - hasNext
        - items
      properties:
        lastCardId:
          allOf:
            - $ref: '#/components/schemas/CardId'
          description: Id of last card returned
          nullable: true
        hasNext:
          description: Whether cards exist after the last one returned
          type: boolean
        items:
          type: array
          items:
            $ref: '#/components/schemas/Card'

    GetCardResponse:
      $ref: '#/components/schemas/Card'

    AddCardRequest:
      type: object
      required:
        - title
        - author
        - content
      properties:
        title:
          type: string
          minLength: 1
        author:
          type: string
          minLength: 1
        content:
          type: string
          minLength: 1

    AddCommentRequest:
      type: object
      required:
        - title
        - author
        - content
      properties:
        title:
          type: string
          minLength: 1
        author:
          type: string
          minLength: 1
        content:
          type: string
          minLength: 1

    GetCommentsResponse:
      type: object
      required:
        - lastCommentId
        - hasNext
        - items
      properties:
        lastCommentId:
          allOf:
            - $ref: '#/components/schemas/CommentId'
          description: Id of last comment returned
          nullable: true
        hasNext:
          description: Whether comments exist after the last one returned
          type: boolean
        items:
          type: array
          items:
            $ref: '#/components/schemas/Comment'

    Card:
      type: object
      required:
        - cardId
        - title
        - author
        - dateCreated
        - likesCount
        - commentsCount
        - content
      properties:
        cardId:
          $ref: '#/components/schemas/CardId'
        title:
          type: string
          minLength: 1
        author:
          type: string
          minLength: 1
        dateCreated:
          type: string
          format: date-time
        likesCount:
          type: integer
          minimum: 0
        commentsCount:
          type: integer
          minimum: 0
        content:
          type: string
          minLength: 1

    Comment:
      type: object
      required:
        - commentId
        - author
        - dateCreated
        - content
        - likesCount
      properties:
        comment:
          $ref: '#/components/schemas/CommentId'
        author:
          type: string
          minLength: 1
        dateCreated:
          type: string
          format: date-time
        content:
          type: string
          minLength: 1
        likesCount:
          type: integer
          minimum: 0

    CardId:
      description: Card identifier
      type: string
      format: uuid

    CommentId:
      description: Comment identifier
      type: string
      format: uuid

    ErrorResponse:
      description: Response in case of error
      type: object
      required:
        - type
        - message
      properties:
        type:
          type: string
          minLength: 1
          example: UserNotFound
        message:
          type: string
          minLength: 1
          example: The user you requested was not found, try another one
        details:
          type: object
