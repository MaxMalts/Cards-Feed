/*
 * Cards Feed
 * This is a cards feed
 *
 * OpenAPI spec version: 1.0.0-SNAPSHOT
 * Contact: maltsev.ms@phystech.edu
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 3.0.50
 *
 * Do not edit the class manually.
 *
 */
import {ApiClient} from '../ApiClient';

/**
 * The AddCommentRequest model module.
 * @module model/AddCommentRequest
 * @version 1.0.0-SNAPSHOT
 */
export class AddCommentRequest {
  /**
   * Constructs a new <code>AddCommentRequest</code>.
   * @alias module:model/AddCommentRequest
   * @class
   * @param title {String} 
   * @param author {String} 
   * @param content {String} 
   */
  constructor(title, author, content) {
    this.title = title;
    this.author = author;
    this.content = content;
  }

  /**
   * Constructs a <code>AddCommentRequest</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/AddCommentRequest} obj Optional instance to populate.
   * @return {module:model/AddCommentRequest} The populated <code>AddCommentRequest</code> instance.
   */
  static constructFromObject(data, obj) {
    if (data) {
      obj = obj || new AddCommentRequest();
      if (data.hasOwnProperty('title'))
        obj.title = ApiClient.convertToType(data['title'], 'String');
      if (data.hasOwnProperty('author'))
        obj.author = ApiClient.convertToType(data['author'], 'String');
      if (data.hasOwnProperty('content'))
        obj.content = ApiClient.convertToType(data['content'], 'String');
    }
    return obj;
  }
}

/**
 * @member {String} title
 */
AddCommentRequest.prototype.title = undefined;

/**
 * @member {String} author
 */
AddCommentRequest.prototype.author = undefined;

/**
 * @member {String} content
 */
AddCommentRequest.prototype.content = undefined;

