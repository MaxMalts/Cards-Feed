/*
 * Cards Feed
 * This is a cards feed
 *
 * OpenAPI spec version: 1.0.0-SNAPSHOT
 * Contact: maltsev.ms@phystech.edu
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 3.0.50
 *
 * Do not edit the class manually.
 *
 */
import {ApiClient} from '../ApiClient';
import {CardId} from './CardId';

/**
 * The AllOfGetCardsResponseLastCardId model module.
 * @module model/AllOfGetCardsResponseLastCardId
 * @version 1.0.0-SNAPSHOT
 */
export class AllOfGetCardsResponseLastCardId extends CardId {
  /**
   * Constructs a new <code>AllOfGetCardsResponseLastCardId</code>.
   * Id of last card returned
   * @alias module:model/AllOfGetCardsResponseLastCardId
   * @class
   * @extends module:model/CardId
   */
  constructor() {
    super();
  }

  /**
   * Constructs a <code>AllOfGetCardsResponseLastCardId</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/AllOfGetCardsResponseLastCardId} obj Optional instance to populate.
   * @return {module:model/AllOfGetCardsResponseLastCardId} The populated <code>AllOfGetCardsResponseLastCardId</code> instance.
   */
  static constructFromObject(data, obj) {
    if (data) {
      obj = obj || new AllOfGetCardsResponseLastCardId();
      CardId.constructFromObject(data, obj);
    }
    return obj;
  }
}
