# CardsFeed.CardApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addCard**](CardApi.md#addCard) | **POST** /card | addCard
[**getCard**](CardApi.md#getCard) | **GET** /card/{cardId} | getCard
[**getCards**](CardApi.md#getCards) | **GET** /cards | getCards
[**likeCard**](CardApi.md#likeCard) | **POST** /card/{cardId}/like | likeCard
[**unlikeCard**](CardApi.md#unlikeCard) | **POST** /card/{cardId}/unlike | unlikeCard

<a name="addCard"></a>
# **addCard**
> addCard(body, xAppName)

addCard

Create a new card

### Example
```javascript
import {CardsFeed} from 'cards_feed';
let defaultClient = CardsFeed.ApiClient.instance;

// Configure API key authorization: apiKey
let apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

let apiInstance = new CardsFeed.CardApi();
let body = new CardsFeed.AddCardRequest(); // AddCardRequest | 
let xAppName = "xAppName_example"; // String | Calling application name

apiInstance.addCard(body, xAppName, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AddCardRequest**](AddCardRequest.md)|  | 
 **xAppName** | **String**| Calling application name | 

### Return type

null (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCard"></a>
# **getCard**
> GetCardResponse getCard(xAppName, cardId)

getCard

Returns card data

### Example
```javascript
import {CardsFeed} from 'cards_feed';
let defaultClient = CardsFeed.ApiClient.instance;

// Configure API key authorization: apiKey
let apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

let apiInstance = new CardsFeed.CardApi();
let xAppName = "xAppName_example"; // String | Calling application name
let cardId = new CardsFeed.CardId(); // CardId | Card identifier

apiInstance.getCard(xAppName, cardId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAppName** | **String**| Calling application name | 
 **cardId** | [**CardId**](.md)| Card identifier | 

### Return type

[**GetCardResponse**](GetCardResponse.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCards"></a>
# **getCards**
> GetCardsResponse getCards(xAppName, limit, opts)

getCards

Returns cards

### Example
```javascript
import {CardsFeed} from 'cards_feed';
let defaultClient = CardsFeed.ApiClient.instance;

// Configure API key authorization: apiKey
let apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

let apiInstance = new CardsFeed.CardApi();
let xAppName = "xAppName_example"; // String | Calling application name
let limit = 56; // Number | Maximum number of cards returned
let opts = { 
  'cursor': new CardsFeed.CardId() // CardId | Id of previous card (not included in response)
};
apiInstance.getCards(xAppName, limit, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAppName** | **String**| Calling application name | 
 **limit** | **Number**| Maximum number of cards returned | 
 **cursor** | [**CardId**](.md)| Id of previous card (not included in response) | [optional] 

### Return type

[**GetCardsResponse**](GetCardsResponse.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="likeCard"></a>
# **likeCard**
> likeCard(xAppName, cardId)

likeCard

Add like on a card

### Example
```javascript
import {CardsFeed} from 'cards_feed';
let defaultClient = CardsFeed.ApiClient.instance;

// Configure API key authorization: apiKey
let apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

let apiInstance = new CardsFeed.CardApi();
let xAppName = "xAppName_example"; // String | Calling application name
let cardId = new CardsFeed.CardId(); // CardId | Card identifier

apiInstance.likeCard(xAppName, cardId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAppName** | **String**| Calling application name | 
 **cardId** | [**CardId**](.md)| Card identifier | 

### Return type

null (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="unlikeCard"></a>
# **unlikeCard**
> unlikeCard(xAppName, cardId)

unlikeCard

Remove like from a card

### Example
```javascript
import {CardsFeed} from 'cards_feed';
let defaultClient = CardsFeed.ApiClient.instance;

// Configure API key authorization: apiKey
let apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

let apiInstance = new CardsFeed.CardApi();
let xAppName = "xAppName_example"; // String | Calling application name
let cardId = new CardsFeed.CardId(); // CardId | Card identifier

apiInstance.unlikeCard(xAppName, cardId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAppName** | **String**| Calling application name | 
 **cardId** | [**CardId**](.md)| Card identifier | 

### Return type

null (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

