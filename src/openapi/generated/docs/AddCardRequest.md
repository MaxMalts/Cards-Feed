# CardsFeed.AddCardRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | 
**author** | **String** |  | 
**content** | **String** |  | 
