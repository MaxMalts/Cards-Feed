# CardsFeed.CommentApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addComment**](CommentApi.md#addComment) | **POST** /card/{cardId}/comment | addComment
[**getComments**](CommentApi.md#getComments) | **GET** /card/{cardId}/comments | getComments
[**likeComment**](CommentApi.md#likeComment) | **POST** /comment/{commentId}/like | likeComment
[**unlikeComment**](CommentApi.md#unlikeComment) | **POST** /comment/{commentId}/unlike | unlikeComment

<a name="addComment"></a>
# **addComment**
> addComment(body, xAppName, cardId)

addComment

Create a new comment

### Example
```javascript
import {CardsFeed} from 'cards_feed';
let defaultClient = CardsFeed.ApiClient.instance;

// Configure API key authorization: apiKey
let apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

let apiInstance = new CardsFeed.CommentApi();
let body = new CardsFeed.AddCommentRequest(); // AddCommentRequest | 
let xAppName = "xAppName_example"; // String | Calling application name
let cardId = new CardsFeed.CardId(); // CardId | Card identifier

apiInstance.addComment(body, xAppName, cardId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AddCommentRequest**](AddCommentRequest.md)|  | 
 **xAppName** | **String**| Calling application name | 
 **cardId** | [**CardId**](.md)| Card identifier | 

### Return type

null (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getComments"></a>
# **getComments**
> GetCommentsResponse getComments(xAppName, cardId, limit, opts)

getComments

Get comments on a card

### Example
```javascript
import {CardsFeed} from 'cards_feed';
let defaultClient = CardsFeed.ApiClient.instance;

// Configure API key authorization: apiKey
let apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

let apiInstance = new CardsFeed.CommentApi();
let xAppName = "xAppName_example"; // String | Calling application name
let cardId = new CardsFeed.CardId(); // CardId | Card identifier
let limit = 56; // Number | Maximum number of cards returned
let opts = { 
  'cursor': new CardsFeed.CommentId() // CommentId | Id of previous comment (not included in response)
};
apiInstance.getComments(xAppName, cardId, limit, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAppName** | **String**| Calling application name | 
 **cardId** | [**CardId**](.md)| Card identifier | 
 **limit** | **Number**| Maximum number of cards returned | 
 **cursor** | [**CommentId**](.md)| Id of previous comment (not included in response) | [optional] 

### Return type

[**GetCommentsResponse**](GetCommentsResponse.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="likeComment"></a>
# **likeComment**
> likeComment(xAppName, commentId)

likeComment

Add like on a comment

### Example
```javascript
import {CardsFeed} from 'cards_feed';
let defaultClient = CardsFeed.ApiClient.instance;

// Configure API key authorization: apiKey
let apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

let apiInstance = new CardsFeed.CommentApi();
let xAppName = "xAppName_example"; // String | Calling application name
let commentId = new CardsFeed.CommentId(); // CommentId | Comment identifier

apiInstance.likeComment(xAppName, commentId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAppName** | **String**| Calling application name | 
 **commentId** | [**CommentId**](.md)| Comment identifier | 

### Return type

null (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="unlikeComment"></a>
# **unlikeComment**
> unlikeComment(xAppName, commentId)

unlikeComment

Remove like from a comment

### Example
```javascript
import {CardsFeed} from 'cards_feed';
let defaultClient = CardsFeed.ApiClient.instance;

// Configure API key authorization: apiKey
let apiKey = defaultClient.authentications['apiKey'];
apiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.apiKeyPrefix = 'Token';

let apiInstance = new CardsFeed.CommentApi();
let xAppName = "xAppName_example"; // String | Calling application name
let commentId = new CardsFeed.CommentId(); // CommentId | Comment identifier

apiInstance.unlikeComment(xAppName, commentId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xAppName** | **String**| Calling application name | 
 **commentId** | [**CommentId**](.md)| Comment identifier | 

### Return type

null (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

