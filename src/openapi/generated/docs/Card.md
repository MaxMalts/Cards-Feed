# CardsFeed.Card

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cardId** | [**CardId**](CardId.md) |  | 
**title** | **String** |  | 
**author** | **String** |  | 
**dateCreated** | **Date** |  | 
**likesCount** | **Number** |  | 
**commentsCount** | **Number** |  | 
**content** | **String** |  | 
