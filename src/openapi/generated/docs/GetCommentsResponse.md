# CardsFeed.GetCommentsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lastCommentId** | **AllOfGetCommentsResponseLastCommentId** | Id of last comment returned | 
**hasNext** | **Boolean** | Whether comments exist after the last one returned | 
**items** | [**[Comment]**](Comment.md) |  | 
