# CardsFeed.Comment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | [**CommentId**](CommentId.md) |  | [optional] 
**author** | **String** |  | 
**dateCreated** | **Date** |  | 
**content** | **String** |  | 
**likesCount** | **Number** |  | 
