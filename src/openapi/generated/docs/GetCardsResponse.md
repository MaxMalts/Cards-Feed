# CardsFeed.GetCardsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lastCardId** | **AllOfGetCardsResponseLastCardId** | Id of last card returned | 
**hasNext** | **Boolean** | Whether cards exist after the last one returned | 
**items** | [**[Card]**](Card.md) |  | 
