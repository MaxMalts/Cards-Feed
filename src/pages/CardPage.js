import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';

import {connect} from 'react-redux';
import {actionLoadCards} from '@store/actions/cardsActions';

import Card from '@components/Card';
import NotFound from '@pages/NotFound.js';
import {getCards} from '@assets/server-mocks/getCards';

import styles from './CardPage.module.scss';

const mapDispatchToProps = dispatch => ({
    loadCards: cards => dispatch(actionLoadCards(cards))
});

function CardPage({loadCards}) {
    const cardId = parseInt(useParams().id);

    let [notFound, setNotFound] = useState(false);
    let [loading, setLoading] = useState(true);

    useEffect(() => {
        if (Number.isNaN(cardId)) {
            setNotFound(true);
        }

        getCards().then(cards => {
            loadCards(cards);
            if (cards.some(item => item.id === cardId)) {
                console.log(`${new Date().toString()}: visited the card with id ${cardId}.`);
            } else {
                setNotFound(true);
            }
            setLoading(false);
        });
    }, [cardId, loadCards]);

    return notFound
        ? <NotFound/>
        : (
            <main className={styles.cardContainer}>
                {loading
                    ? 'Loading...'
                    : <Card id={cardId} synaptic={false}/>
                }
            </main>
        );
}

export default connect(null, mapDispatchToProps)(CardPage);