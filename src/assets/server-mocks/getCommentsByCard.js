import comments from '@assets/server-mocks/data/comments.json'
import emulateError from '@assets/server-mocks/emulateError.js';

const COMMENTS_LOAD_DURATION = 1000;

function selectByCardId(totalData, id) {
    return totalData.filter(({cardId}) => cardId === id)
}

export async function getComments(cardId) {
    return new Promise(resolve => {
        emulateError();

        const targetComments = selectByCardId(comments, cardId)
        setTimeout(() => resolve(targetComments), COMMENTS_LOAD_DURATION)
    })
}