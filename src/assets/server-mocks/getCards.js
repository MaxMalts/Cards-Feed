import cards from '@assets/server-mocks/data/cards.json'
import emulateError from '@assets/server-mocks/emulateError.js';

const CARDS_LOAD_DURATION = 1500;

export async function getCards() {
    return new Promise(resolve => {
        emulateError();
        setTimeout(() => resolve(cards), CARDS_LOAD_DURATION)
    })
}